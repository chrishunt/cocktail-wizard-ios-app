//
//  CocktailListTableViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 20/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CocktailDataModel.h"
#import "CocktailCell.h"
#import "AppDelegate.h"
#import "CocktailViewController.h"

//Class to be extended to represent a list of cocktails, can be segmented or not
@interface CocktailListTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *data;
@property (weak, nonatomic) CocktailDataModel *model;

@end
