//
//  CocktailOptionsViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 21/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrAllManager.h"

//Manages segues to different cocktail list viewss
@interface CocktailOptionsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property (weak, nonatomic) IBOutlet UIButton *allCocktailsButton;
@property (weak, nonatomic) IBOutlet UIButton *nearlyThereButton;
@property (weak, nonatomic) IBOutlet UIButton *rectentButton;
@property (weak, nonatomic) IBOutlet UIButton *myCocktailButton;

@end
