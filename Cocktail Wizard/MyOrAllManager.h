//
//  MyOrAllManager.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 23/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <Foundation/Foundation.h>

//Singleton class to represent whether a user is in "my cocktails" or "all cocktails" to avoid repeated classes, as I could find no other method to pass information into TabBarItems from the segue.
@interface MyOrAllManager : NSObject

@property (nonatomic) BOOL *allCocktails;

+(id)sharedManager;

@end
