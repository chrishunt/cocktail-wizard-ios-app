//
//  CocktailDataModel.m
//  Cocktail Wizard
//
//  Created by Adam Reece Spencer Poulston on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailDataModel.h"
#import <sqlite3.h>

@implementation CocktailDataModel

-(id)initWithDBPath:(NSString*)dbPath {
    self = [super init];
    if (self) {
        _dbPath = dbPath;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
////
////   Internet connectivity operations - from <NSURLConnectionDelegate> protocol
////
//////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, init the response data
    // Get the response data so we can check the header types later on
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    if ([response respondsToSelector:@selector(allHeaderFields)]) {
        _contentType = [httpResponse allHeaderFields];
    }
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil, no cache
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // All data received
    // Parse the content based on the header
    
    //if Json, check the identifying key from the api and send to the relevant parsing method
    if([[_contentType objectForKey:@"Content-Type"] isEqualToString:@"application/json"]) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:_responseData options:kNilOptions error:&error];
        if ([json count] > 1) {
            //check the "beingupdated" key to see what is to be parsed
            NSString* beingParsed = [json objectForKey:@"beingupdated"];
            if ([beingParsed isEqualToString:@"cocktails"]) {
                //cocktails
                [self updateCocktailsWithDictionary:json];
            } else if ([beingParsed isEqualToString:@"ingredients"]) {
                //ingredients
                [self updateIngredientsWithDictionary:json];
            } else if ([beingParsed isEqualToString:@"cocktailtypes"]) {
                //cocktail types- not implemented, left in for future extensibility
            } else if ([beingParsed isEqualToString:@"ingredienttypes"]) {
                //ingredient types- not implemented, left in for future extensibility
            } else if ([beingParsed isEqualToString:@"images"]) {
                //images
                [self updateImagesWithDictionary:json];
            }
            
        }
    } else {
        //If json is not returned, we are looking at an image, store it
        NSString * currentURL = [NSString stringWithFormat:@"%@",[connection currentRequest]];
        //get the filename from the url
        NSArray *parts = [[currentURL substringToIndex:currentURL.length-(currentURL.length>0)] componentsSeparatedByString:@"/"];
        //filename is the final part of the url
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        
        //get the document path and store the image
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:filename];
        UIImage* theImage = [UIImage imageWithData:_responseData];
        [UIImagePNGRepresentation(theImage) writeToFile:dataPath atomically:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason
    NSLog(@"An error occurred");
}

//Taken from http://stackoverflow.com/questions/11314819/how-to-check-for-network-reachability-on-ios-in-a-non-blocking-manner
//Uses CFNetworking library to check if a connection is available to the service
- (BOOL)isNetworkAvailable {
    CFNetDiagnosticRef diag;
    diag = CFNetDiagnosticCreateWithURL (NULL, (__bridge CFURLRef)[NSURL URLWithString:@"http://cocktailservice.adampoulston.co.uk/"]);
    
    CFNetDiagnosticStatus status;
    status = CFNetDiagnosticCopyNetworkStatusPassively (diag, NULL);
    
    CFRelease (diag);
    
    if ( status == kCFNetDiagnosticConnectionUp )
    {
        NSLog (@"Connection is up");
        return YES;
    } else {
        NSLog (@"Connection is down");
        return NO;
        
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
////
////   Passive update operations
////
//////////////////////////////////////////////////////////////////////////////////////////////////////


// Performs requests to update the relevant data used in the app
-(void)requestToUpdateService:(NSString*)options {
    
    //get the date of the last update, this way only new or updated information is received
    NSString* theLastUpdate = [self datetimeOfLastUpdate];
    
    // Create the requests
    
    //request to the cocktail updating parameter
    NSURLRequest *cocktailRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"http://cocktailservice.adampoulston.co.uk/service.php?service=updatecocktails&lastupdate=%@", theLastUpdate] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    //request to the ingredient updating parameter
    NSURLRequest *ingredientRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"http://cocktailservice.adampoulston.co.uk/service.php?service=updateingredients&lastupdate=%@", theLastUpdate] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    
    //request to the image updating parameter
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"http://cocktailservice.adampoulston.co.uk/service.php?service=updateimages&lastupdate=%@", theLastUpdate] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    
    // Create url connections and fire off the requests
    [NSURLConnection connectionWithRequest:cocktailRequest delegate:self];
    
    [NSURLConnection connectionWithRequest:ingredientRequest delegate:self];
    
    [NSURLConnection connectionWithRequest:imageRequest delegate:self];
}

//get the time of the last update, used to ensure only relevant information is downloaded
-(NSString*)datetimeOfLastUpdate {
    NSString* theTime = @"1970-01-01 00:00:00";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the cocktail
        const char *luSqlStatement = [[NSString stringWithFormat:@"SELECT at FROM lastupdate LIMIT 1"] UTF8String];
        sqlite3_stmt *luCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, luSqlStatement, -1, &luCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(luCompiledStatement)==SQLITE_ROW) {
                //get the ingredient id and add it to the array
                theTime = [NSString stringWithUTF8String:(char*)sqlite3_column_text(luCompiledStatement, 0)];
            }
        }
        // release the compiled statement
        sqlite3_finalize(luCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    [self setNewLastUpdate];
    return theTime;
}

//used to parse the updated cocktail information json serialized into a dictionary.
-(void)updateCocktailsWithDictionary:(NSDictionary*)dict {
    //iterate through the keys
    for(id key in dict)
    {
        //ignore the being-updated key, it is for identification only.
        if (![key isEqualToString:@"beingupdated"]) {
            //get the dictionary representing one cocktail
            NSDictionary* aCocktail = [dict objectForKey:key];
            //pull the relevant information from the cocktail dict, and call the method to update or insert the cocktail into the database
            [self updateCocktailWithID:[key intValue] name:[aCocktail objectForKey:@"name"] ingredients:[aCocktail objectForKey:@"ingredients"] recipe:[aCocktail objectForKey:@"recipe"] cocktailType:[[aCocktail objectForKey:@"type"] intValue] image:[[aCocktail objectForKey:@"hasImage"] intValue]];
        }
    }
}

//used to update or insert a cocktail into the database by update functionality
-(void)updateCocktailWithID:(NSInteger)cocktailID name:(NSString*)name ingredients:(NSArray*)ingredients recipe:(NSString*)recipe cocktailType:(NSInteger)cocktailType image:(NSInteger)image {
    //See if cocktail already exists in database
    Cocktail* toCheck = [self getCocktailById:cocktailID];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to update or insert cocktail based on whether toCheck is nil (isnt in database
        const char *cuSqlStatement = (!toCheck) ?  [[NSString stringWithFormat:@"INSERT INTO cocktails(ID, name, recipe, cocktailTypeID, hasImage) VALUES (%d, '%@', '%@', %d, %d)", cocktailID, name, recipe, cocktailType, image] UTF8String] : [[NSString stringWithFormat:@"UPDATE cocktails SET name = '%@', recipe = '%@', cocktailTypeID = %d, hasImage= %d WHERE ID = %d", name, recipe, cocktailType, image, cocktailID] UTF8String];
        
        sqlite3_stmt *cuCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cuSqlStatement, -1, &cuCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(cuCompiledStatement);
            //if all goes well, we also need to link the ingredients to the cocktail
            [self updateCocktailIngredientLinkerWithArray:ingredients cocktailID:cocktailID];
        }
        // release the compiled statement
        sqlite3_finalize(cuCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
}

//updates or insert the links between a cocktail and ingredients in the cocktail ingredient linker table
-(void)updateCocktailIngredientLinkerWithArray:(NSMutableArray*)ingredients cocktailID:(NSInteger)cocktailID {
    //clear all current links
    [self removeCocktailIngredientLink:cocktailID];
    sqlite3 *database;
    //add a new link for  the new or updated ingredients
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        for (id ingID in ingredients){
        //open the connection
            //SQL statement to get insert the link
            const char *cilSqlStatement = [[NSString stringWithFormat:@"INSERT INTO cocktailIngredientLinker VALUES (%d,%d)", cocktailID,[ingID intValue]] UTF8String];
            sqlite3_stmt *cilCompiledStatement;
            //compile the statement
            if (sqlite3_prepare_v2(database, cilSqlStatement, -1, &cilCompiledStatement, NULL)==SQLITE_OK) {
                sqlite3_step(cilCompiledStatement);
            }
            // release the compiled statement
            sqlite3_finalize(cilCompiledStatement);
            // close the database
            
        }
        
    }
    sqlite3_close(database);
}

//clears the links to ingredients for a specific cocktail
-(void)removeCocktailIngredientLink:(NSInteger)cocktailID {
        sqlite3 *database;
        //open the connection
        if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
            //SQL statement to delete all links for the specified cocktail id
            const char *clSqlStatement = [[NSString stringWithFormat:@"DELETE FROM cocktailIngredientLinker WHERE cocktailID = %d", cocktailID] UTF8String];
            sqlite3_stmt *clCompiledStatement;
            //compile the statement
            if (sqlite3_prepare_v2(database, clSqlStatement, -1, &clCompiledStatement, NULL)==SQLITE_OK) {
                sqlite3_step(clCompiledStatement);
            }
            // release the compiled statement
            sqlite3_finalize(clCompiledStatement);
            // close the database
            sqlite3_close(database);
            
            
        }
}

//used to parse the updated ingredient information json serialized into a dictionary. Very similar to cocktails
-(void)updateIngredientsWithDictionary:(NSDictionary*)dict {
    //iterate through keys
    for(id key in dict)
    {
        //ignore identifier flag
        if (![key isEqualToString:@"beingupdated"]) {
            //make call to update or insert the ingredients
            NSDictionary* anIng = [dict objectForKey:key];
            [self updateIngredientWithID:[key intValue] name:[anIng objectForKey:@"name"] ingredientTypeID:[[anIng objectForKey:@"type"] intValue] image:[[anIng objectForKey:@"hasImage"] intValue]];
        }
    }
}

//used to update or insert a new ingredient into the database.
-(void)updateIngredientWithID:(NSInteger)ingredientID name:(NSString*)name ingredientTypeID:(NSInteger)ingredientTypeID image:(NSInteger)image {
    sqlite3 *database;
    //open the connection
    //check if ingredient exists
    Ingredient* toCheck = [self getIngredientById:ingredientID];
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to update or insert the ingredient
        const char *cuSqlStatement = (!toCheck) ?  [[NSString stringWithFormat:@"INSERT INTO ingredients(ID, name, ingredientTypeID, hasImage, inBar) VALUES (%d, '%@', %d, %d, %d)", ingredientID, name, ingredientTypeID, image, 0] UTF8String] : [[NSString stringWithFormat:@"UPDATE ingredients SET name = '%@', ingredientTypeID=%d, hasImage=%d WHERE ID = %d", name, ingredientTypeID, image, ingredientID] UTF8String];
        sqlite3_stmt *cuCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cuSqlStatement, -1, &cuCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(cuCompiledStatement);
        }
        // release the compiled statement
        sqlite3_finalize(cuCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
}

//method not implemented in this release, should future functionality ot add new types of cocktails be decided up, this will be implemented
-(void)updateCocktailTypesWithDictionary:(NSDictionary*)dict {
    
}

//method not implemented in this release, should future functionality ot add new types of ingredients be decided up, this will be implemented
-(void)updateIngredientTypesWithDictionary:(NSDictionary*)dict {
    
}

//used to parse the updated image information json serialized into a dictionary. imilar to cocktails and images
-(void)updateImagesWithDictionary:(NSDictionary*)dict {
    for(id key in dict)
    {
        if (![key isEqualToString:@"beingupdated"]) {
            //make the call to update the images
            NSDictionary* anImage = [dict objectForKey:key];
            [self updateImageWithID:[key intValue] name:[anImage objectForKey:@"name"] URL:[anImage objectForKey:@"url"]];
        }
    }
}

//update or insert the new image info into the database
-(void)updateImageWithID:(NSInteger)imageID name:(NSString*)name URL:(NSString*)URL {
    sqlite3 *database;
    //open the connection
    NSString* toCheck = [self getImageNameByID:imageID];
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to update or insert the image - mark image as uninitialised
        const char *cuSqlStatement = ([toCheck isEqualToString:@"not_found.png"]) ?  [[NSString stringWithFormat:@"INSERT INTO images(ID, name, URL, initialised) VALUES (%d, '%@', '%@', 0)", imageID, name, URL] UTF8String] : [[NSString stringWithFormat:@"UPDATE images SET name = '%@', url='%@', initialised = 0 WHERE ID = %d", name, URL, imageID] UTF8String];
        sqlite3_stmt *cuCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cuSqlStatement, -1, &cuCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(cuCompiledStatement);
        }
        // release the compiled statement
        sqlite3_finalize(cuCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    
    //initialise the image for the first time - attempts to download the image to the device if internet is available
    (void) [self initialiseImagebyID:imageID];
}

//register the new time of update, for future reference
-(void)setNewLastUpdate {
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to set the new date
        const char *luSqlStatement = [[NSString stringWithFormat:@"UPDATE lastupdate SET at = datetime()"] UTF8String];
        sqlite3_stmt *luCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, luSqlStatement, -1, &luCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(luCompiledStatement);
        }
        // release the compiled statement
        sqlite3_finalize(luCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
}




//////////////////////////////////////////////////////////////////////////////////////////////////////
////
////   Cocktail getting from sqlite
////
//////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSMutableArray*)getAllCocktails {
    //array of cocktails to return
    NSMutableArray* allCocktails = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all cocktails
        const char *cockSqlStatement = "SELECT * FROM cocktails";
        sqlite3_stmt *cockCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                // read the data columns from the result row
                //get the cocktail id
                NSInteger cockId = sqlite3_column_int(cockCompiledStatement,0);
                
                //get the name
                NSString *name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 1)];

                //get the recipe in it unparsed form
                NSString *recipeUnparsed = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 2)];
                
                //get the cocktail type as a number, needs converting
                NSInteger cType = sqlite3_column_int(cockCompiledStatement,3);
                
                //get the hasImage flag, is an int of 0 or 1.
                NSInteger hasImageID = sqlite3_column_int(cockCompiledStatement,4);
                
                //get the ingredients
                NSMutableArray* ingredients = [[NSMutableArray alloc] init];
                //call method to get the links
                NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                for(NSNumber* anIngId in ingredientIds) {
                    [ingredients addObject:[self getIngredientById:[anIngId integerValue]]];
                }
                
                //parse the recipe
                NSMutableArray* recipeParsed = [self parseRecipe:recipeUnparsed];
                                
                //get the cocktail type
                //convert from int to text
                NSString *coType = [self getCocktailTypeById:cType];
                
                //get the hasImage flag
                NSString* imageName = @"default_cocktail.png";
                if(hasImageID > 0)
                    imageName = [self initialiseImagebyID:hasImageID];
                
                // create a new cocktail
                Cocktail *c = [[Cocktail alloc] initWithID:cockId name:name ingredients:ingredients recipe:recipeParsed cocktailType:coType image:imageName favourite:[self isCocktailFavourited:cockId]];
                
                // insert it into the array
                [allCocktails addObject:c];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    //sort the array alphabetically on name
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [allCocktails sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return allCocktails;
}

//get a list of all the cocktail IDs
-(NSMutableArray*)getAllCocktailIDs
{
    NSMutableArray* cocktailIDs = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all cocktails
        const char *cockSqlStatement = "SELECT ID FROM cocktails";
        sqlite3_stmt *cockCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                // read the data columns from the result row
                //get the cocktail id
                NSNumber *cockId = [[NSNumber alloc] initWithInteger:sqlite3_column_int(cockCompiledStatement,0)];
                
                //add it to the array
                [cocktailIDs addObject:cockId];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    return cocktailIDs;
}

//get cocktail at random from the databeast
-(Cocktail*)getRandomCocktail
{
    
    NSMutableArray *cocktailIDs = [self getAllCocktailIDs];
    
    if ([cocktailIDs count] == 0) {
        //cant return a cocktail if none exist!
        return nil;
    }
    else
    {
        //otherwise choose an id at random from the full list
        NSInteger randID = [[cocktailIDs objectAtIndex: arc4random() % [cocktailIDs count]] integerValue];
        
        //then get the cocktail
        Cocktail *randomCocktail = [self getCocktailById:randID];
        return randomCocktail;
    }
}

//get a cocktail object based on an ID
-(Cocktail*)getCocktailById:(NSInteger)cockId {
    //cocktail to return
    Cocktail* cocktail = nil;
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the cocktail
        const char *cockSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM cocktails WHERE ID = '%d'", cockId] UTF8String];
        sqlite3_stmt *cockCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                // read the data columns from the result row
                //get the cocktail id
                NSInteger cockId = sqlite3_column_int(cockCompiledStatement,0);
                
                //get the name
                NSString *name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 1)];
                
                //get the recipe in it unparsed form
                NSString *recipeUnparsed = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 2)];
                
                //get the cocktail type as a number, needs converting
                NSInteger cType = sqlite3_column_int(cockCompiledStatement,3);
                
                //get the hasImage flag, is an int of 0 or 1.
                NSInteger hasImageID = sqlite3_column_int(cockCompiledStatement,4);
                
                //get the ingredients
                NSMutableArray* ingredients = [[NSMutableArray alloc] init];
                NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                for(NSNumber* anIngId in ingredientIds) {
                    [ingredients addObject:[self getIngredientById:[anIngId integerValue]]];
                }
                
                //parse the recipe
                NSMutableArray* recipeParsed = [self parseRecipe:recipeUnparsed];
                
                //get the cocktail type
                //convert from int to text
                NSString *coType = [self getCocktailTypeById:cType];
                
                //get the hasImage flag
                NSString* imageName = @"default_cocktail.png";
                if(hasImageID > 0)
                    imageName = [self initialiseImagebyID:hasImageID];
                
                // create the new cocktail object
                cocktail = [[Cocktail alloc] initWithID:cockId name:name ingredients:ingredients recipe:recipeParsed cocktailType:coType image:imageName favourite:[self isCocktailFavourited:cockId]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    
    return cocktail;
}

//get all of the ingredients linked to a particular cocktail
-(NSMutableArray*)getCocktailIngredientsByCocktailId:(NSInteger)cockId {
    //array of ingredients to return
    NSMutableArray* cockIngs = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the cocktail
        const char *cockIngSqlStatement = [[NSString stringWithFormat:@"SELECT ingredientID FROM cocktailIngredientLinker WHERE cocktailID = '%d'", cockId] UTF8String];
        sqlite3_stmt *cockIngCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockIngSqlStatement, -1, &cockIngCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(cockIngCompiledStatement)==SQLITE_ROW) {
                //get the ingredient id and add it to the array
                NSInteger ingId = sqlite3_column_int(cockIngCompiledStatement,0);

                [cockIngs addObject:[NSNumber numberWithInteger:ingId]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockIngCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    
    return cockIngs;
}

//get an inrgedient object based on an ingredient ID
-(Ingredient*)getIngredientById:(NSInteger)ingId {
    Ingredient* ingredient = nil;
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the ingredient
        const char *ingSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM ingredients WHERE ID = '%d' LIMIT 1", ingId] UTF8String];
        sqlite3_stmt *ingCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, ingSqlStatement, -1, &ingCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(ingCompiledStatement)==SQLITE_ROW) {
                //get the ingredient and begin to construct the object
                NSInteger hasImageID = sqlite3_column_int(ingCompiledStatement,3);
                
                //is in bar?
                NSInteger inBar = sqlite3_column_int(ingCompiledStatement,4);
                
                //get the name
                NSString *name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(ingCompiledStatement, 1)];
                
                //get the type
                NSString *ingType = [self getIngredientTypeById:ingId];
                
                //get the name of the image and initialise it if needs be
                NSString* imageName = @"bottle.jpg";//@"default_ingredient.png";
                if(hasImageID > 0)
                    imageName = [self initialiseImagebyID:hasImageID];
                
                //store whether ingredient is in bar or not
                BOOL inBarFlag = NO;
                if(inBar > 0)
                    inBarFlag = YES;
                
                //construct the inrgedient object
                ingredient = [[Ingredient alloc] initWithID:ingId name:name ingredientType:ingType image:imageName inBar:inBarFlag];
            }
        }
        // release the compiled statement
        sqlite3_finalize(ingCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return ingredient;
}

//gets an ingredient object based on a name string
-(Ingredient*)getIngredientByName:(NSString*)name {
    Ingredient* ingredient = nil;
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the ingredient
        const char *ingSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM ingredients WHERE name = '%@' LIMIT 1", name] UTF8String];
        sqlite3_stmt *ingCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, ingSqlStatement, -1, &ingCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(ingCompiledStatement)==SQLITE_ROW) {
                //get the ingredient id
                NSInteger ingId = sqlite3_column_int(ingCompiledStatement,0);
                
                //get image
                NSInteger hasImageID = sqlite3_column_int(ingCompiledStatement,3);
                
                //check if in bar
                NSInteger inBar = sqlite3_column_int(ingCompiledStatement,4);
                
                //get the name
                NSString *name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(ingCompiledStatement, 1)];
                
                //get the ingredient type
                NSString *ingType = [self getIngredientTypeById:ingId];
                
                //get the image name
                NSString* imageName = @"bottle.jpg";//@"default_ingredient.png";
                if(hasImageID > 0)
                    imageName = [self initialiseImagebyID:hasImageID];
                
                //check if in bar
                BOOL inBarFlag = NO;
                if(inBar > 0)
                    inBarFlag = YES;
                
                //init the ingredient
                ingredient = [[Ingredient alloc] initWithID:ingId name:name ingredientType:ingType image:imageName inBar:inBarFlag];
            }
        }
        // release the compiled statement
        sqlite3_finalize(ingCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return ingredient;
}


//get a cocktail tpye based on an id
-(NSString*)getCocktailTypeById:(NSInteger)cockTypeId {
    NSString* cockType = @"Type Not Found";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the cocktail type
        const char *cockTypeSqlStatement = [[NSString stringWithFormat:@"SELECT name FROM cocktailTypes WHERE ID = '%d' LIMIT 1", cockTypeId] UTF8String];
        sqlite3_stmt *cockTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockTypeSqlStatement, -1, &cockTypeCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(cockTypeCompiledStatement)==SQLITE_ROW) {
                //return the type name
                cockType = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockTypeCompiledStatement, 0)];
                
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return cockType;
}

//get ingredient type based on an id
-(NSString*)getIngredientTypeById:(NSInteger)ingTypeId {
    NSString* ingType = @"Type Not Found";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the type
        const char *ingTypeSqlStatement = [[NSString stringWithFormat:@"SELECT name FROM ingredientTypes WHERE ID = '%d' LIMIT 1", ingTypeId] UTF8String];
        sqlite3_stmt *ingTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, ingTypeSqlStatement, -1, &ingTypeCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(ingTypeCompiledStatement)==SQLITE_ROW) {
                //get the ingredient type to return
                ingType = [NSString stringWithUTF8String:(char*)sqlite3_column_text(ingTypeCompiledStatement, 0)];
                
            }
        }
        // release the compiled statement
        sqlite3_finalize(ingTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return ingType;
}

//gets the id of an ingredient type based on its name
-(NSInteger)getIngredientIdByType:(NSString*)ingType {
    NSInteger ingID = 0;
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the id
        const char *ingTypeSqlStatement = [[NSString stringWithFormat:@"SELECT ID FROM ingredientTypes WHERE name = '%@' LIMIT 1", ingType] UTF8String];
        sqlite3_stmt *ingTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, ingTypeSqlStatement, -1, &ingTypeCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(ingTypeCompiledStatement)==SQLITE_ROW) {
                //get the ingredient type id and add it to the array
                ingID = sqlite3_column_int(ingTypeCompiledStatement,0);
                
            }
        }
        // release the compiled statement
        sqlite3_finalize(ingTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return ingID;
}

//gets the id of a cocktail type based on its name
-(NSInteger)getCocktailIdByType:(NSString*)cockType {
    NSInteger cockID = 0;
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the id
        const char *cockTypeSqlStatement = [[NSString stringWithFormat:@"SELECT ID FROM cocktailTypes WHERE name = '%@' LIMIT 1", cockType] UTF8String];
        sqlite3_stmt *cockTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockTypeSqlStatement, -1, &cockTypeCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(cockTypeCompiledStatement)==SQLITE_ROW) {
                //get the cocktail type id
                cockID = sqlite3_column_int(cockTypeCompiledStatement,0);
                
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return cockID;
}

//method to parse the recipe text stored in the database to a displayable form
-(NSMutableArray*)parseRecipe:(NSString*)recipe {
    //want nsmutable array of nsstrings
    //usual recipe form: <step>Get a glass.</step><step>Put in vodka.</step><step>Put in cola.</step><step>Weep softly.</step>
   
    //regex to get text between step tags
    NSRange   searchedRange = NSMakeRange(0, [recipe length]);
    NSString *pattern = @"<step>(.*?)</step>";
    NSError  *error = nil;
    
    //run the regex
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray* matches = [regex matchesInString:recipe options:0 range: searchedRange];
    NSMutableArray *steps = [[NSMutableArray alloc] init];
    NSString *step = nil;
    
    //add the matches to the return array
    for (NSTextCheckingResult* match in matches) {
        NSRange group1 = [match rangeAtIndex:1];
        step = [recipe substringWithRange:group1];
        [steps addObject:step];
    }
    return steps;
}

//this method is used to get cocktails a user has the ingredients for, and is also used to check for cocktails that have a number of ingredients missing
//USAGE: number is how many ingredients are missing, ie 0 is complete match, 1 means 1 missing ingredient
-(NSMutableArray*)getCocktailsByNumberOfMissingIngredients:(NSInteger)number {
    //array of cocktails to return
    NSMutableArray* matchedCocktails = [[NSMutableArray alloc] init];
    
    //get a list of cocktails ownded by the user for comparison
    NSMutableArray* ownedIngredients = [self getOwnedIngredients];
    
    if ([ownedIngredients count] > 0) {
        sqlite3 *database;
        //open the connection
        if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
            //SQL statement to get all cocktails
            const char *cockSqlStatement = "SELECT * FROM cocktails";
            sqlite3_stmt *cockCompiledStatement;
            //compile the statement
            if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
                while (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                    // read the data columns from the result row
                    //get the cocktail id
                    NSInteger cockId = sqlite3_column_int(cockCompiledStatement,0);
                    
                    //get the ingredients for the cocktail
                    NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                    int matches = 0;
                    
                    //compare ownded ingredients to cocktail ingredients, log a match if one is found
                    for (NSNumber *ownedIng in ownedIngredients) {
                        for (NSNumber *checkIng in ingredientIds) {
                            if([ownedIng integerValue] == [checkIng integerValue]) {
                                matches++;
                            }
                        }
                    }
                    
                    if (([ingredientIds count] - matches) ==  number) {
                        //get the name
                        NSString *name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 1)];
                        
                        //get the recipe in it unparsed form
                        NSString *recipeUnparsed = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 2)];
                        //get the cocktail type as a number, needs converting
                        NSInteger cType = sqlite3_column_int(cockCompiledStatement,3);
                        
                        //get the hasImage flag, is an int of 0 or 1.
                        NSInteger hasImageID = sqlite3_column_int(cockCompiledStatement,4);
                        
                        //get the ingredients
                        NSMutableArray* ingredients = [[NSMutableArray alloc] init];
                        NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                        for(NSNumber* anIngId in ingredientIds) {
                            [ingredients addObject:[self getIngredientById:[anIngId integerValue]]];
                        }
                        
                        //parse the recipe
                        NSMutableArray* recipeParsed = [self parseRecipe:recipeUnparsed];
                        
                        //get the cocktail type
                        //convert from int to text
                        NSString *coType = [self getCocktailTypeById:cType];
                        
                        //get the hasImage flag
                        NSString* imageName = @"default_cocktail.png";
                        if(hasImageID > 0)
                            imageName = [self initialiseImagebyID:hasImageID];
                        
                        // create a new cocktail
                        Cocktail *c = [[Cocktail alloc] initWithID:cockId name:name ingredients:ingredients recipe:recipeParsed cocktailType:coType image:imageName favourite:[self isCocktailFavourited:cockId]];
                        
                        // insert it into the array
                        [matchedCocktails addObject:c];
                    }
                }
            }
            // release the compiled statement
            sqlite3_finalize(cockCompiledStatement);
            // close the database
            sqlite3_close(database);
        }
    } else if([ownedIngredients count] == 0) {
        sqlite3 *database;
        //SQL statement to get all cocktails

        //compile the statement
        if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
            const char *cockSqlStatement = "SELECT ID FROM cocktails";
            sqlite3_stmt *cockCompiledStatement;
            if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
                while (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                    // read the data columns from the result row
                    //get the cocktail id
                    NSInteger cockId = sqlite3_column_int(cockCompiledStatement,0);
                    
                    //get the ingredients for the cocktail
                    NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                    if([ingredientIds count] == number)
                    {
                        [matchedCocktails addObject:[self getCocktailById:cockId]];
                    }
                }
            }
            // release the compiled statement
            sqlite3_finalize(cockCompiledStatement);
            // close the database
            sqlite3_close(database);
        }
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [matchedCocktails sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return matchedCocktails;
}

//same as previous method but alspo filters by type
-(NSMutableArray*)getCocktailsByNumberOfMissingIngredients:(NSInteger)number type:(NSString*)type {
    //array of cocktails to return
    NSMutableArray* matchedCocktails = [[NSMutableArray alloc] init];
    
    //get a list of cocktails ownded by the user for comparison
    NSMutableArray* ownedIngredients = [self getOwnedIngredients];
    
    if ([ownedIngredients count] > 0) {
        sqlite3 *database;
        //open the connection
        if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
            //SQL statement to get all cocktails
            const char *cockSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM cocktails WHERE cocktailTypeID = %d", [self getCocktailIdByType:type]] UTF8String];
            sqlite3_stmt *cockCompiledStatement;
            //compile the statement
            if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
                while (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                    // read the data columns from the result row
                    //get the cocktail id
                    NSInteger cockId = sqlite3_column_int(cockCompiledStatement,0);
                    
                    //get the ingredients for the cocktail
                    NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                    int matches = 0;
                    
                    //compare ownded ingredients to cocktail ingredients, log a match if one is found
                    for (NSNumber *ownedIng in ownedIngredients) {
                        for (NSNumber *checkIng in ingredientIds) {
                            if([ownedIng integerValue] == [checkIng integerValue]) {
                                matches++;
                            }
                        }
                    }
                    
                    if (([ingredientIds count] - matches) ==  number) {
                        //get the name
                        NSString *name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 1)];
                        
                        //get the recipe in it unparsed form
                        NSString *recipeUnparsed = [NSString stringWithUTF8String:(char*)sqlite3_column_text(cockCompiledStatement, 2)];
                        //get the cocktail type as a number, needs converting
                        NSInteger cType = sqlite3_column_int(cockCompiledStatement,3);
                        
                        //get the hasImage flag, is an int of 0 or 1.
                        NSInteger hasImageID = sqlite3_column_int(cockCompiledStatement,4);
                        
                        //get the ingredients
                        NSMutableArray* ingredients = [[NSMutableArray alloc] init];
                        NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                        for(NSNumber* anIngId in ingredientIds) {
                            [ingredients addObject:[self getIngredientById:[anIngId integerValue]]];
                        }
                        
                        //parse the recipe
                        NSMutableArray* recipeParsed = [self parseRecipe:recipeUnparsed];
                        
                        //get the cocktail type -- another SQL query, unless we load a map of index to types on startup
                        //convert from int to text
                        NSString *coType = [self getCocktailTypeById:cType];
                        
                        //get the hasImage flag
                        NSString* imageName = @"default_cocktail.png";
                        if(hasImageID > 0)
                            imageName = [self initialiseImagebyID:hasImageID];
                        // create a new cocktail
                        Cocktail *c = [[Cocktail alloc] initWithID:cockId name:name ingredients:ingredients recipe:recipeParsed cocktailType:coType image:imageName favourite:[self isCocktailFavourited:cockId]];
                        //[c print];
                        // insert it into the array
                        [matchedCocktails addObject:c];
                    }
                }
            }
            // release the compiled statement
            sqlite3_finalize(cockCompiledStatement);
            // close the database
            sqlite3_close(database);
        }
    } else if([ownedIngredients count] == 0) {
        sqlite3 *database;
        //SQL statement to get all cocktails
        
        //compile the statement
        if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
            const char *cockSqlStatement = [[NSString stringWithFormat:@"SELECT ID FROM cocktails WHERE cocktailTypeID = %d", [self getCocktailIdByType:type]] UTF8String];
            sqlite3_stmt *cockCompiledStatement;
            if (sqlite3_prepare_v2(database, cockSqlStatement, -1, &cockCompiledStatement, NULL)==SQLITE_OK) {
                while (sqlite3_step(cockCompiledStatement)==SQLITE_ROW) {
                    // read the data columns from the result row
                    //get the cocktail id
                    NSInteger cockId = sqlite3_column_int(cockCompiledStatement,0);
                    
                    //get the ingredients for the cocktail
                    NSMutableArray* ingredientIds = [self getCocktailIngredientsByCocktailId:cockId];
                    if([ingredientIds count] == number)
                    {
                        [matchedCocktails addObject:[self getCocktailById:cockId]];
                    }
                }
            }
            // release the compiled statement
            sqlite3_finalize(cockCompiledStatement);
            // close the database
            sqlite3_close(database);
        }
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [matchedCocktails sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return matchedCocktails;
}

//same as getCocktailsByNumberOfMissingIngredients:(NSInteger)number but only returns cocktails containing a specific ingredient
-(NSMutableArray*)getCocktailsByNumberOfMissingIngredients:(NSInteger)number ingredientName:(NSString*)ingredientName {
    Ingredient* toMatch  = [self getIngredientByName:ingredientName];
    NSMutableArray* matchedCocktails = [[NSMutableArray alloc] init];
    NSMutableArray* ownedIngredients = [self getOwnedIngredients];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the cocktail
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT cocktailID FROM cocktailIngredientLinker WHERE ingredientID = '%d'", toMatch.ingredientID] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //get the ingredient id and add it to the array
                int matches = 0;
                Cocktail* toCheck = [self getCocktailById:sqlite3_column_int(imgCompiledStatement,0)];
                
                //compare ownded ingredients to cocktail ingredients, log a match if one is found
                for (NSNumber *ownedIng in ownedIngredients) {
                    for (Ingredient *checkIng in toCheck.ingredients) {
                        if([ownedIng integerValue] == checkIng.ingredientID) {
                            matches++;
                        }
                    }
                }
                
                //add the cocktail to the return list if it is a match
                if (([toCheck.ingredients count] - matches) ==  number) {
                    [matchedCocktails addObject:toCheck];
                }
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
    }

    
    return matchedCocktails;
}

//gets a list of cocktails containing a specific ingredient
-(NSMutableArray*)getCocktailsByIngredientName:(NSString*)ingredientName {
    Ingredient* toMatch  = [self getIngredientByName:ingredientName];
    NSMutableArray* matchedCocktails = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the relevant cocktails
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT cocktailID FROM cocktailIngredientLinker WHERE ingredientID = '%d'", toMatch.ingredientID] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //get the cocktail id and add the relevant cocktail object to the array
                [matchedCocktails addObject:[self getCocktailById:sqlite3_column_int(imgCompiledStatement,0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    
    
    return matchedCocktails;
}

//get a list of cocktail types
-(NSMutableArray*)getCocktailTypes {
    NSMutableArray* types = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the cocktail
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT name FROM cocktailTypes"] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //get the cocktail type name and add it to thwe array
                [types addObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(imgCompiledStatement, 0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    
    
    return types;
}

//get list of ingredients owned in my bar
-(NSMutableArray*)getOwnedIngredients {
    //array of ingredients to return
    NSMutableArray* myIngredients = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all cocktails
        const char *owningSqlStatement = "SELECT * FROM ingredients WHERE inBar = 1";
        sqlite3_stmt *owningCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, owningSqlStatement, -1, &owningCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(owningCompiledStatement)==SQLITE_ROW) {
                // read the data columns from the result row
                //get the cingredient id
                NSInteger ingId = sqlite3_column_int(owningCompiledStatement,0);
                // insert it into the array
                [myIngredients addObject:[NSNumber numberWithInteger:ingId]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(owningCompiledStatement);
        // close the database
        sqlite3_close(database);
    }

    return myIngredients;
}

//gets the name of an image, e.g. foo.jpg, based on an image id
-(NSMutableArray*)getOwnedIngredientObjects {
    //array of cocktails to return
    NSMutableArray* myIngredients = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all cocktails
        const char *owningSqlStatement = "SELECT * FROM ingredients WHERE inBar = 1";//[[NSString
        sqlite3_stmt *owningCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, owningSqlStatement, -1, &owningCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(owningCompiledStatement)==SQLITE_ROW) {
                // read the data columns from the result row
                //get the cocktail id
                NSInteger ingId = sqlite3_column_int(owningCompiledStatement,0);
                // insert it into the array
                [myIngredients addObject:[self getIngredientById:ingId]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(owningCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
    
    return myIngredients;
}

-(NSString*)getImageNameByID:(NSInteger)hasImageID {
    NSString* imageName = @"not_found.png";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the image name
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT name FROM images WHERE ID = '%d' LIMIT 1", hasImageID] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //init the image name string to return
                imageName = [NSString stringWithUTF8String:(char*)sqlite3_column_text(imgCompiledStatement, 0)];
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return imageName;
}

//UNUSED - method to get image associated with cocktail type
-(NSString*)getImageByCocktailTypeName:(NSString *)cocktailTypeName {
    NSString* imageName = @"default_cocktail_type.png";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the cocktail
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT hasImage FROM cocktailTypes WHERE name = '%@' LIMIT 1", cocktailTypeName] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //get the ingredient id and add it to the array
                NSInteger hasImageID = sqlite3_column_int(imgCompiledStatement,0);
                if (hasImageID > 0) {
                    imageName = [self getImageNameByID:hasImageID];
                }
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return imageName;
}


//UNUSED - method to get image associated with ingredient type
-(NSString*)getImageByIngredientTypeName:(NSString *)ingredientTypeName {
    NSString* imageName = @"default_ingredient_type.png";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the cocktail
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT hasImage FROM imageTypes WHERE name = '%@' LIMIT 1", ingredientTypeName] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //get the ingredient id and add it to the array
                NSInteger hasImageID = sqlite3_column_int(imgCompiledStatement,0);
                if (hasImageID > 0) {
                    imageName = [self getImageNameByID:hasImageID];
                }
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return imageName;
}


//toggles whether an ingredient is owned or not
-(void)toggleInMyBar:(Ingredient *)ingredient {
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //check the current status
        NSInteger newVal = 0;
        if (ingredient.inBar == NO) {
            //if not in bar, make it so
            ingredient.inBar = YES;
            newVal = 1;
        } else {
            //otherwise remove it
            ingredient.inBar = NO;
        }
        
        //SQL statement to update the in bar flag of the ingredient
        const char *imgSqlStatement = [[NSString stringWithFormat:@"UPDATE ingredients SET inBar = %d WHERE ID = %d", newVal,ingredient.ingredientID] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(imgCompiledStatement);
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
        
    }
    
}

//gets a list of ingredients that are of a certain type
-(NSMutableArray*)getIngredientsByType:(NSString *)type {
    //get the id of the type
    NSInteger ingTypeId = [self getIngredientIdByType:type];
    NSMutableArray* theIngredients = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients associated with the ingredient type
        const char *ingTypeSqlStatement = [[NSString stringWithFormat:@"SELECT ID FROM ingredients WHERE ingredientTypeID = '%d'", ingTypeId] UTF8String];
        sqlite3_stmt *ingTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, ingTypeSqlStatement, -1, &ingTypeCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(ingTypeCompiledStatement)==SQLITE_ROW) {
                //get the ingredient and add it to the array
                [theIngredients addObject:[self getIngredientById:sqlite3_column_int(ingTypeCompiledStatement,0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(ingTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [theIngredients sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return theIngredients;
}

//returns a list off all ingredients
-(NSMutableArray*)getAllIngredients {
    NSMutableArray* theIngredients = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all ingredients
        const char *ingTypeSqlStatement = [[NSString stringWithFormat:@"SELECT ID FROM ingredients"] UTF8String];
        sqlite3_stmt *ingTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, ingTypeSqlStatement, -1, &ingTypeCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(ingTypeCompiledStatement)==SQLITE_ROW) {
                //get the ingredient and add it to the array
                [theIngredients addObject:[self getIngredientById:sqlite3_column_int(ingTypeCompiledStatement,0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(ingTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    //sort by name alphabetically
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [theIngredients sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return theIngredients;
}

//gets a list of all cocktails associated with a certain type
-(NSMutableArray*)getCocktailsByType:(NSString *)type {
    //get the id of the type
    NSInteger cockTypeId = [self getCocktailIdByType:type];
    NSMutableArray* theCocktails = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all cocktails associated with the type id
        const char *cockTypeSqlStatement = [[NSString stringWithFormat:@"SELECT ID FROM cocktails WHERE cocktailTypeID = '%d'", cockTypeId] UTF8String];
        sqlite3_stmt *cockTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockTypeSqlStatement, -1, &cockTypeCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(cockTypeCompiledStatement)==SQLITE_ROW) {
                //get the ingredient and add it to the array
                [theCocktails addObject:[self getCocktailById:sqlite3_column_int(cockTypeCompiledStatement,0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    //sort by name alphabertically
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [theCocktails sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return theCocktails;
}

//method to check if a cocktail is favourited
-(BOOL)isCocktailFavourited:(NSInteger)cocktailID {
    BOOL favourited = NO;
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the row of the favourited cocktail if it exists
        const char *favSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM favourites WHERE cocktailID = '%d'", cocktailID] UTF8String];
        sqlite3_stmt *favCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, favSqlStatement, -1, &favCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(favCompiledStatement)==SQLITE_ROW) {
                //if it exists it is favourited
                favourited = YES;
            }
        }
        // release the compiled statement
        sqlite3_finalize(favCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return favourited;
}

//inverts the favourited nature of a cocktail
-(void)toggleFavouriteCocktail:(Cocktail*)cocktail {
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        
        //SQL statement to change a cocktail state. if it is favourited unfavourite it, otherwise favourite
        const char *favSqlStatement = (cocktail.favourite) ? [[NSString stringWithFormat:@"DELETE FROM favourites WHERE cocktailID = %d",cocktail.cocktailID] UTF8String] : [[NSString stringWithFormat:@"INSERT INTO favourites VALUES (%d)",cocktail.cocktailID] UTF8String];
        sqlite3_stmt *favCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, favSqlStatement, -1, &favCompiledStatement, NULL)==SQLITE_OK) {
            //mark the object as the inverse of its prior favourite state
            cocktail.favourite = !cocktail.favourite;
            sqlite3_step(favCompiledStatement);
        }
        // release the compiled statement
        sqlite3_finalize(favCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
}

//marks a cocktail as having been viewed recently. if it is viewed again it is pushed back up the list. only 10 recent cocktails are stored
-(void)addToRecent:(Cocktail*)cocktail {
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        
        //SQL statement to either update the view date of a cocktail if it is in the list, or insert it if not
        const char *recSqlStatement = ([self isRecentCocktail:cocktail]) ? [[NSString stringWithFormat:@"UPDATE recent SET `at` = datetime('now') WHERE cocktailID = %d",cocktail.cocktailID] UTF8String] : [[NSString stringWithFormat:@"INSERT INTO recent (cocktailID) VALUES (%d)",cocktail.cocktailID] UTF8String];

        sqlite3_stmt *recCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, recSqlStatement, -1, &recCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(recCompiledStatement);
            //see if more than 10 entries, delete the oldest if there is
            const char *limitSqlStatement = [[NSString stringWithFormat:@"DELETE FROM recent WHERE cocktailID NOT IN (SELECT cocktailID FROM recent ORDER BY at DESC LIMIT 10)"] UTF8String];
            sqlite3_stmt *limitCompiledStatement;
            //compile the statement
            if (sqlite3_prepare_v2(database, limitSqlStatement, -1, &limitCompiledStatement, NULL)==SQLITE_OK) {
                //see if more than 10 entries
                sqlite3_step(limitCompiledStatement);
            }
            // release the compiled statement
            sqlite3_finalize(limitCompiledStatement);
            //finished adding recent cocktail
        }
        // release the compiled statement
        sqlite3_finalize(recCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
}

//checks if a cocktail has been viewed recently
-(BOOL)isRecentCocktail:(Cocktail*)cocktail {
    BOOL recent = NO;
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to see if the cocktail is in the recent cocktails table
        const char *recSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM recent WHERE cocktailID = %d", cocktail.cocktailID] UTF8String];
        sqlite3_stmt *recCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, recSqlStatement, -1, &recCompiledStatement, NULL)==SQLITE_OK) {
            if (sqlite3_step(recCompiledStatement)==SQLITE_ROW) {
                //yes if it is found
                recent = YES;
            }
        }
        // release the compiled statement
        sqlite3_finalize(recCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    return recent;
}

//gets a list of recently viewed cocktails
-(NSMutableArray*)getRecentCocktails {
    NSMutableArray* theCocktails = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to the recently view cocktails as IDs
        const char *cockTypeSqlStatement = [[NSString stringWithFormat:@"SELECT cocktailID FROM recent ORDER BY `at` DESC"] UTF8String];
        sqlite3_stmt *cockTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockTypeSqlStatement, -1, &cockTypeCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(cockTypeCompiledStatement)==SQLITE_ROW) {
                //get the cocktail from the id and add it to the array
                [theCocktails addObject:[self getCocktailById:sqlite3_column_int(cockTypeCompiledStatement,0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }

    return theCocktails;
}

//gets a list of the users favourited cocktails
-(NSMutableArray*)getFavouriteCocktails {
    NSMutableArray* theCocktails = [[NSMutableArray alloc] init];
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get all cocktail IDs listed in favourites
        const char *cockTypeSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM favourites"] UTF8String];
        sqlite3_stmt *cockTypeCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cockTypeSqlStatement, -1, &cockTypeCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(cockTypeCompiledStatement)==SQLITE_ROW) {
                //get the cocktail objects for the ids
                [theCocktails addObject:[self getCocktailById:sqlite3_column_int(cockTypeCompiledStatement,0)]];
            }
        }
        // release the compiled statement
        sqlite3_finalize(cockTypeCompiledStatement);
        // close the database
        sqlite3_close(database);
        
        
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [theCocktails sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return theCocktails;
}

//method to initialise images based on an id
//if the image is in the bundle (URL = inBundle), the image will be copied the the device and marked as initialised.
//if the image is external, the method checks for a viable internet connection, and if one is present, copies the image to the device.
//if the image is marked as already initialised in the DB, the name of the image is returned for displaying, and no copying is done.
-(NSString*)initialiseImagebyID:(NSInteger)imageID {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    //image IDs that have been edited, and need marking as initialised
    NSMutableArray* dirty = [[NSMutableArray alloc] init];
    
    //now get the image details
    NSString* url = @"notFound";
    NSString* imgFile = @"bottle.jpg";//@"default.jpg";
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to get the images info
        const char *imgSqlStatement = [[NSString stringWithFormat:@"SELECT * FROM images WHERE ID = %d",imageID] UTF8String];
        sqlite3_stmt *imgCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, imgSqlStatement, -1, &imgCompiledStatement, NULL)==SQLITE_OK) {
            while (sqlite3_step(imgCompiledStatement)==SQLITE_ROW) {
                //if the image is initialised, just return the image name for displaying
                if (sqlite3_column_int(imgCompiledStatement, 3) < 1) {
                    //otherwise get its url for copying purposes
                    url = [NSString stringWithUTF8String:(char*)sqlite3_column_text(imgCompiledStatement, 2)];
                    
                    //if the images is in the application bundle
                    if([url isEqualToString:@"inBundle"]) {
                        //get from bundle
                        imgFile = [NSString stringWithUTF8String:(char*)sqlite3_column_text(imgCompiledStatement, 1)];
                        NSString *imgPathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imgFile];
                        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:imgFile];
                        //copy to device
                        [fileManager copyItemAtPath:imgPathFromApp toPath:filePath error:nil];
                        //mark image as initialised at end of method
                        [dirty addObject:[NSNumber numberWithInt:sqlite3_column_int(imgCompiledStatement, 0)]];
                    
                    } else {
                        //otherwise we need to pull from the remote service
                        if ([self isNetworkAvailable]) {
                            //if the URL is reachable, make the async request
                            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[NSString stringWithString:url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                            [NSURLConnection connectionWithRequest:imageRequest delegate:self];
                            //get the name of the file to return
                            imgFile = [NSString stringWithUTF8String:(char*)sqlite3_column_text(imgCompiledStatement, 1)];
                            //mark the file to be marked as initialised
                            [dirty addObject:[NSNumber numberWithInt:sqlite3_column_int(imgCompiledStatement, 0)]];
                        }
                    }
                } else {
                    //just return the name if it is already initialised
                    imgFile = [NSString stringWithUTF8String:(char*)sqlite3_column_text(imgCompiledStatement, 1)];
                }
                
            }
        }
        // release the compiled statement
        sqlite3_finalize(imgCompiledStatement);
        // close the database
        sqlite3_close(database);
        
    }
    
    //mark updated image files as initialised
    for(NSNumber* dirtImage in dirty) {
        [self markImageAsInitialisedByID:[dirtImage integerValue]];
    }
    
    return imgFile;
}

//marks an image as having been initialised, ie file copied to phone
-(void)markImageAsInitialisedByID:(NSInteger)imageID {
    sqlite3 *database;
    //open the connection
    if (sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        //SQL statement to update the image as inited
        const char *cuSqlStatement = [[NSString stringWithFormat:@"UPDATE images SET `initialised`=1 WHERE ID = %d",imageID] UTF8String];
        sqlite3_stmt *cuCompiledStatement;
        //compile the statement
        if (sqlite3_prepare_v2(database, cuSqlStatement, -1, &cuCompiledStatement, NULL)==SQLITE_OK) {
            sqlite3_step(cuCompiledStatement);
        }
        // release the compiled statement
        sqlite3_finalize(cuCompiledStatement);
        // close the database
        sqlite3_close(database);
    }
}

@end
