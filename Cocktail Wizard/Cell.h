//
//  Cell.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 10/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell : UICollectionViewCell

@property (retain, nonatomic) UILabel* label;

@end
