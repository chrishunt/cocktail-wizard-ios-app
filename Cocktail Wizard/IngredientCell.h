//
//  IngredientCell.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 10/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ingredient.h"
#import "CocktailDataModel.h"

//Reresents an ingredient in my bar
@interface IngredientCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ingImage;
@property (weak, nonatomic) IBOutlet UILabel *ingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tickImage;
@property (nonatomic) BOOL isSelected;
@property (weak) Ingredient *ing;
@property (weak,nonatomic) CocktailDataModel *model;

-(void)select;
-(void)deselect;
@end
