//
//  MyOrAllManager.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 23/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "MyOrAllManager.h"

@implementation MyOrAllManager

+ (id)sharedManager {
    static MyOrAllManager *sharedMyOrAllManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyOrAllManager = [[self alloc] init];
    });
    return sharedMyOrAllManager;
}

- (id)init {
    if (self = [super init]) {
        self.allCocktails = NO;
    }
    return self;
}

- (void)dealloc {
    // Should never be called
}
@end
