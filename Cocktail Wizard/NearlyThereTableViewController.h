//
//  NearlyThereTableViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 21/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailListTableViewController.h"

//View controller displaying cocktails which a user is one, two, and three ingredients away from being able to make
@interface NearlyThereTableViewController : CocktailListTableViewController

typedef enum Sections : NSInteger Sections;
enum Sections : NSInteger
{
    ONEOFF,
    TWOOFF,
    THREEOFF
};

@end
