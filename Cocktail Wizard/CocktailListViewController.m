//
//  CocktailListViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 23/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailListViewController.h"

@interface CocktailListViewController ()

@end

@implementation CocktailListViewController

-(void)viewWillAppear:(BOOL)animated
{
    //Gets singleton class holding whether a user has selected "my" or "all cocktails"
    MyOrAllManager *sharedManager = [MyOrAllManager sharedManager];
    
    //Loads either all or makeable cocktails and sets appropriate title
    self.data = [[NSMutableArray alloc] init];
    if (sharedManager.allCocktails) {
        [self.data addObject:[self.model getAllCocktails]];
        self.parentViewController.navigationItem.title = @"All Cocktails";
    } else {
        [self.data addObject:[self.model getCocktailsByNumberOfMissingIngredients:0]];
        self.parentViewController.navigationItem.title = @"My Cocktails";
    }
    [self.tableView reloadData];
}

//Segue to selected cocktail
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"cocktailListSegue"])
    {
        CocktailViewController *c = [segue destinationViewController];
        CocktailCell *selectedCell = (CocktailCell*)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        Cocktail *cocktail = selectedCell.cocktail;
        c.cocktail = cocktail;
        
    }
}

@end
