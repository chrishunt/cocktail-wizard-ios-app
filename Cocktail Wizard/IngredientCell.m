//
//  IngredientCell.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 10/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "IngredientCell.h"

@implementation IngredientCell

//Shows tick and toggles as owned in the database
-(void)select
{
    [self.tickImage setHidden:NO];
    self.isSelected = YES;
    [self.model toggleInMyBar:self.ing];
}

//Hides tick and toggles as unowned in the database
-(void)deselect
{
    [self.tickImage setHidden: YES];
    self.isSelected = NO;
    [self.model toggleInMyBar:self.ing];
}

@end