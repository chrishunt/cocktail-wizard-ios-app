//
//  Cocktail.h
//  Cocktail Wizard
//
//  Created by Adam Reece Spencer Poulston on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ingredient.h"

//Class to represent an individual cocktail
@interface Cocktail : NSObject

@property (assign) NSInteger cocktailID;
@property (strong) NSString* name;
@property (strong) NSMutableArray* ingredients;
@property (strong) NSMutableArray* recipe; //array of steps
@property (strong) NSString* cocktailType;
@property (strong) NSString* image;
@property (assign) BOOL favourite;

-(id)initWithID:(NSInteger)cocktailID name:(NSString*)name ingredients:(NSMutableArray*)ingredients recipe:(NSMutableArray*)recipe cocktailType:(NSString*)cocktailType image:(NSString*)image favourite:(BOOL)isFavourite;
-(NSMutableArray*)getIngredientNames;
-(NSString*)cocktailIngredientsToString;
@end
