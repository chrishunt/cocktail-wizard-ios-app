//
//  NearlyThereTableViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 21/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "NearlyThereTableViewController.h"

@interface NearlyThereTableViewController ()

@end

@implementation NearlyThereTableViewController

-(void)viewWillAppear:(BOOL)animated
{
    //Loads cocktails which are 1,2 and 3 ingredients off being makeable
    self.data = [[NSMutableArray alloc] init];
    for (NSInteger i=1; i <= 3; i++) {
        NSMutableArray *cocktails =  [self.model getCocktailsByNumberOfMissingIngredients:i];
        [self.data addObject:cocktails];
    }
    [self.tableView reloadData];
}

//Loads section headers
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case ONEOFF:
            return @"One ingredient off";
            break;
        case TWOOFF:
            return @"Two ingredients off";
            break;
        case THREEOFF:
            return @"Three ingredients off";
            break;
        default:
            return @"";
            break;
    }
}

//Segues to selected cocktail
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"nearlyThereSegue"])
    {
        CocktailViewController *c = [segue destinationViewController];
        CocktailCell *selectedCell = (CocktailCell*)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        Cocktail *cocktail = selectedCell.cocktail;
        c.cocktail = cocktail;
    }
}

@end