//
//  CocktailListViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 23/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailListTableViewController.h"
#import "MyOrAllManager.h"

//View controller to represent a list of either all cocktails or cocktails that a user can make

@interface CocktailListViewController : CocktailListTableViewController

@end
