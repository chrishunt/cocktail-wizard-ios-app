//
//  MyBarViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 10/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IngredientCell.h"
#import "CocktailDataModel.h"
#import "AppDelegate.h"

//View controller managing my bar
@interface MyBarViewController : UICollectionViewController

@property (weak) CocktailDataModel *model;

//Ingredients of this tab bar item
@property (strong, nonatomic) NSMutableArray *ings;

//Enum for tab bar items
typedef enum MyBarTab : NSInteger MyBarTab;
enum MyBarTab : NSInteger {
    MIXERS,
    ALCOHOL,
    OTHER
};

@end
