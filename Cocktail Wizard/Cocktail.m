//
//  Cocktail.m
//  Cocktail Wizard
//
//  Created by Adam Reece Spencer Poulston on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "Cocktail.h"
#import "Ingredient.h"

@implementation Cocktail

//init method to set up a cocktail object
-(id)initWithID:(NSInteger)cocktailID name:(NSString*)name ingredients:(NSMutableArray*)ingredients recipe:(NSMutableArray*)recipe cocktailType:(NSString*)cocktailType image:(NSString*)image favourite:(BOOL)isFavourite {
    self = [super init];
    if (self) {
        _cocktailID = cocktailID;
        _name = name;
        _ingredients = ingredients;
        _cocktailType = cocktailType;
        _recipe = recipe;
        _image = image;
        _favourite = isFavourite;
    }
    return self;
}

//returns a list of ing name strings, to display on front end
-(NSMutableArray*)getIngredientNames {
    NSMutableArray* names = [[NSMutableArray alloc] init];
    for(Ingredient* ing in self.ingredients) {
        [names addObject:ing.name];
    }
    return names;
}

//returns a string of ingredients for displaying
//e.g. Ing1, Ing2, Ing3
-(NSString*)cocktailIngredientsToString
{
    NSArray *ingstrings = [self.ingredients valueForKey:@"name"];
    NSString *ings = [ingstrings componentsJoinedByString:@", "];
    return ings;
}

@end
