//
//  CocktailDataModel.h
//  Cocktail Wizard
//
//  Created by Adam Reece Spencer Poulston on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cocktail.h"
#import "Ingredient.h"

@interface CocktailDataModel : NSObject<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
    NSDictionary *_contentType;
}

@property (strong) NSString* dbPath;

-(id)initWithDBPath:(NSString*)dbPath;

//cocktail methods
-(Cocktail*)getRandomCocktail;

-(Cocktail*)getCocktailById:(NSInteger)cockId;

-(NSMutableArray*)getCocktailIngredientsByCocktailId:(NSInteger)cockId;

-(NSMutableArray*)parseRecipe:(NSString*)recipe;


////getting lists of cocktails
-(NSMutableArray*)getAllCocktails;

-(NSMutableArray*)getAllCocktailIDs;

-(NSMutableArray*)getCocktailsByNumberOfMissingIngredients:(NSInteger)number;

-(NSMutableArray*)getCocktailsByNumberOfMissingIngredients:(NSInteger)number type:(NSString*)type;

-(NSMutableArray*)getCocktailsByNumberOfMissingIngredients:(NSInteger)number ingredientName:(NSString*)ingredientName;

-(NSMutableArray*)getCocktailsByType:(NSString*)type;

-(NSMutableArray*)getCocktailsByIngredientName:(NSString*)ingredientName;

////favourited cocktails
-(BOOL)isCocktailFavourited:(NSInteger)cocktailID;

-(void)toggleFavouriteCocktail:(Cocktail*)cocktail;

-(NSMutableArray*)getFavouriteCocktails;

////recent cocktails
-(void)addToRecent:(Cocktail*)cocktail;

-(BOOL)isRecentCocktail:(Cocktail*)cocktail;

-(NSMutableArray*)getRecentCocktails;


//cocktail type methods
-(NSString*)getCocktailTypeById:(NSInteger)cockTypeId;

-(NSInteger)getCocktailIdByType:(NSString*)cockType;

-(NSMutableArray*)getCocktailTypes;


//ingredient methods
-(Ingredient*)getIngredientById:(NSInteger)ingId;

-(Ingredient*)getIngredientByName:(NSString*)name;

-(NSMutableArray*)getOwnedIngredients;

-(void)toggleInMyBar:(Ingredient*)ingredient;

-(NSMutableArray*)getIngredientsByType:(NSString*)type;

-(NSMutableArray*)getAllIngredients;

-(NSMutableArray*)getOwnedIngredientObjects;


//ingredient type methods
-(NSString*)getIngredientTypeById:(NSInteger)ingTypeId;

-(NSInteger)getIngredientIdByType:(NSString*)ingType;


//image methods
-(NSString*)getImageNameByID:(NSInteger)hasImageID;

-(NSString*)getImageByCocktailTypeName:(NSString*)cocktailTypeName;

-(NSString*)getImageByIngredientTypeName:(NSString*)ingredientTypeName;

-(NSString*)initialiseImagebyID:(NSInteger)imageID;

-(void)markImageAsInitialisedByID:(NSInteger)imageID;


//networking methods
-(BOOL)isNetworkAvailable;

-(void)requestToUpdateService:(NSString*)options;

-(NSString*)datetimeOfLastUpdate;

-(void)updateCocktailsWithDictionary:(NSDictionary*)dict;

-(void)updateIngredientsWithDictionary:(NSDictionary*)dict;

-(void)updateIngredientWithID:(NSInteger)ingredientID name:(NSString*)name ingredientTypeID:(NSInteger)ingredientTypeID image:(NSInteger)image;

-(void)updateCocktailTypesWithDictionary:(NSDictionary*)dict;

-(void)updateIngredientTypesWithDictionary:(NSDictionary*)dict;

-(void)updateImagesWithDictionary:(NSDictionary*)dict;

-(void)updateImageWithID:(NSInteger)imageID name:(NSString*)name URL:(NSString*)URL;

-(void)updateCocktailWithID:(NSInteger)cocktailID name:(NSString*)name ingredients:(NSMutableArray*)ingredients recipe:(NSString*)recipe cocktailType:(NSInteger)cocktailType image:(NSInteger)image;

-(void)updateCocktailIngredientLinkerWithArray:(NSMutableArray*)ingredients cocktailID:(NSInteger)cocktailID;

-(void)removeCocktailIngredientLink:(NSInteger)cocktailID;

@end
