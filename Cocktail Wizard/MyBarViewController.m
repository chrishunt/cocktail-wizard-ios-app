//
//  MyBarViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 10/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "MyBarViewController.h"

@interface MyBarViewController ()

@end

@implementation MyBarViewController

//Toggles ownership of an ingredient, updating UI and database
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    IngredientCell *ingCell = (IngredientCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (ingCell.isSelected)
    {
        [ingCell deselect];
    }
    else
    {
        [ingCell select];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return self.ings.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IngredientCell *ingCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"IngCell"
                                    forIndexPath:indexPath];
    
    ingCell.model = self.model;
    int row = [indexPath row];
    
    Ingredient *ing = [self.ings objectAtIndex:row];
    
    //Sets ingredient image from the app bundle, or the documents folder
    UIImage *image;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString* filename = [documentsDirectory stringByAppendingPathComponent:ing.image];
    image = [UIImage imageWithContentsOfFile:filename];
    ingCell.ingImage.image = image;
    
    //Set properties
    ingCell.ingLabel.text = ing.name;
    ingCell.isSelected = ing.inBar;
    ingCell.ing = ing;
    
    //Displays or hides the tick as appropriate
    if (ing.inBar)
    {
        [ingCell.tickImage setHidden:NO];
    }
    else
    {
        [ingCell.tickImage setHidden:YES];
    }
   
    ingCell.tickImage.image = [UIImage imageNamed:@"tick.png"];
    
    return ingCell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //Sets background
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"wood.jpg"]];
    
    //Loads model
    self.model = ((AppDelegate*)[UIApplication sharedApplication].delegate).model;
    
    //Gets relevant ingredient types
    NSInteger selectedItemTag = self.tabBarItem.tag;
    
    switch (selectedItemTag)
    {
        case MIXERS:
            self.ings = [self.model getIngredientsByType:@"mixer"];
            break;
        case ALCOHOL:
            self.ings = [self.model getIngredientsByType:@"alcohol"];
            break;
        case OTHER:
            self.ings = [self.model getIngredientsByType:@"other"];
            break;
    }
}

@end