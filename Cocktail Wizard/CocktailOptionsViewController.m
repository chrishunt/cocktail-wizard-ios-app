//
//  CocktailOptionsViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 21/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailOptionsViewController.h"

@interface CocktailOptionsViewController ()

@end

@implementation CocktailOptionsViewController

//Segues to view controller of selected button
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"allCocktailsSegue"])
    {
        MyOrAllManager *sharedManager = [MyOrAllManager sharedManager];
        sharedManager.allCocktails = YES;
    }
    else if ([[segue identifier] isEqualToString:@"myCocktailsSegue"])
    {
        MyOrAllManager *sharedManager = [MyOrAllManager sharedManager];
        sharedManager.allCocktails = NO;
    }
}

@end
