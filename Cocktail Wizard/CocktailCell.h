//
//  CocktailCell.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 21/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cocktail.h"

//Represents a cocktail cell in a list, extends a UITableCell adding a cocktail property
@interface CocktailCell : UITableViewCell

@property (weak, nonatomic) Cocktail *cocktail;

@end
