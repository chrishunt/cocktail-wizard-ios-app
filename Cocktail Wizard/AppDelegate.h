//
//  AppDelegate.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CocktailDataModel.h"
#import "CocktailViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (weak) NSString* databasePath;
@property (weak) NSString* databaseName;
@property (strong) CocktailDataModel* model;
@property (weak, nonatomic) UINavigationController *navController;

- (void)initializeDatabase;

@end
