//
//  CocktailTypeListViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 23/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailTypeListViewController.h"

@interface CocktailTypeListViewController ()

@end

@implementation CocktailTypeListViewController

-(void)viewWillAppear:(BOOL)animated
{
    //Gets singleton class holding whether a user has selected "my" or "all cocktails"
    MyOrAllManager *sharedManager = [MyOrAllManager sharedManager];
    
    //Gets cocktail types
    self.types = [self.model getCocktailTypes];
    self.data = [[NSMutableArray alloc] init];
    
    //For all cocktail types get either user makeable or all cocktails
    for (NSString *type in self.types) {
        if (sharedManager.allCocktails) {
            [self.data addObject:[self.model getCocktailsByType:type]];
        } else {
            [self.data addObject:[self.model getCocktailsByNumberOfMissingIngredients:0 type:type]];
        }
    }
    [self.tableView reloadData];
     
}

//Get cocktail type heading for each section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.types objectAtIndex:section];
}

//Segue to selected cocktail
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"cocktailTypeSegue"])
    {
        CocktailViewController *c = [segue destinationViewController];
        CocktailCell *selectedCell = (CocktailCell*)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        Cocktail *cocktail = selectedCell.cocktail;
        c.cocktail = cocktail;
    }
}

@end