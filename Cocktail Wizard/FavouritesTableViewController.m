//
//  FavouritesTableViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 15/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "FavouritesTableViewController.h"

@interface FavouritesTableViewController ()

@end

@implementation FavouritesTableViewController

-(void)viewWillAppear:(BOOL)animated
{
    //Load favourited cocktails
    NSMutableArray *favourites = [self.model getFavouriteCocktails];
    self.data = [[NSMutableArray alloc] init];
    [self.data addObject:favourites];
    [self.tableView reloadData];
   
}

//Acquire selected cocktail of cell and segue to it
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showSelectedFavourite"])
    {
        CocktailViewController *c = [segue destinationViewController];
        CocktailCell *selectedCell = (CocktailCell*)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        Cocktail *cocktail = selectedCell.cocktail;
        c.cocktail = cocktail;
    }
}

@end