//
//  Ingredient.m
//  Cocktail Wizard
//
//  Created by Adam Reece Spencer Poulston on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "Ingredient.h"

@implementation Ingredient

//method to initialise an image
-(id)initWithID:(NSInteger)ingredientID name:(NSString*)name ingredientType:(NSString*)ingredientType image:(NSString*)image inBar:(BOOL)inBar {
    self = [super init];
    if (self) {
        _ingredientID = ingredientID;
        _name = name;
        _ingredientType = ingredientType;
        _image = image;
        _inBar = inBar;
    }
    return self;
}

@end
