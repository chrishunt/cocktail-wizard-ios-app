//
//  CocktailIngredientListViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 23/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailIngredientListViewController.h"

@interface CocktailIngredientListViewController ()

@end

@implementation CocktailIngredientListViewController

-(void)viewWillAppear:(BOOL)animated
{
    //Gets singleton class holding whether a user has selected "my" or "all cocktails"
    MyOrAllManager *sharedManager = [MyOrAllManager sharedManager];
    
    self.data = [[NSMutableArray alloc] init];
    
    //Load either owned or all ingredients depending on "all" or "my" state
    if (sharedManager.allCocktails)
    {
        self.ingredients = [self.model getAllIngredients];
        
    }
    else
    {
        self.ingredients = [self.model getOwnedIngredientObjects];
    }
    NSMutableArray* ingsWithCocktails = [[NSMutableArray alloc] init];
    
    //Get cocktails for either all or owned ingredients
    for (Ingredient *ingredient in self.ingredients) {
        if (sharedManager.allCocktails) {
            NSMutableArray* theCocktails = [self.model getCocktailsByIngredientName:ingredient.name];
            if ([theCocktails count] != 0) {
                [self.data addObject:theCocktails];
                [ingsWithCocktails addObject:ingredient];
            }
        } else {
            NSMutableArray* theCocktails = [self.model getCocktailsByNumberOfMissingIngredients:0 ingredientName:ingredient.name];
            if ([theCocktails count] != 0) {
                [self.data addObject:theCocktails];
                [ingsWithCocktails addObject:ingredient];
            }
        }
    }
    
    self.ingredients = ingsWithCocktails;
    
    [self.tableView reloadData];
}

//Sets section header for each ingredient
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    Ingredient *ing = [self.ingredients objectAtIndex:section];
    return ing.name;
}

//Segues to selected cocktail
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"cocktailIngredientSegue"])
    {
        CocktailViewController *c = [segue destinationViewController];
        CocktailCell *selectedCell = (CocktailCell*)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        Cocktail *cocktail = selectedCell.cocktail;
        c.cocktail = cocktail;
    }
}

@end