//
//  RecentCocktailsTableViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 22/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "RecentCocktailsTableViewController.h"

@interface RecentCocktailsTableViewController ()

@end

@implementation RecentCocktailsTableViewController

-(void)viewWillAppear:(BOOL)animated
{
    //Loads recently viewed cocktails
    NSMutableArray *recentCocktails = [self.model getRecentCocktails];
    self.data = [[NSMutableArray alloc] init];
    [self.data addObject:recentCocktails];
    [self.tableView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showSelectedRecentCocktail"])
    {
        //Segues to selected cocktail
        CocktailViewController *c = [segue destinationViewController];
        CocktailCell *selectedCell = (CocktailCell*)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        Cocktail *cocktail = selectedCell.cocktail;
        c.cocktail = cocktail;
    }
}

@end
