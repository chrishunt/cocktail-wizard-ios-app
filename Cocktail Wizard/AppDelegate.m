//
//  AppDelegate.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "AppDelegate.h"
#import "CocktailDataModel.h"

@implementation AppDelegate

@synthesize databaseName, databasePath;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    self.databaseName = @"cocktail-wizard.sql";
    
    // get path to the database
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    self.databasePath = [documentsDir stringByAppendingPathComponent:self.databaseName];
    
    
    // initialise the database
    
    [self initializeDatabase];
    
    // Set up the data modesl
    self.model = [[CocktailDataModel alloc] initWithDBPath:self.databasePath];
    
    //check if an internet connection is available, if it is, make a call to the update service
    if ([self.model isNetworkAvailable])
        [self.model requestToUpdateService:@"option"];
    
    //Enable shaking detection
    application.applicationSupportsShakeToEdit = YES;
    
    return YES;
}

//Enables this class to be a responder
-(BOOL)canBecomeFirstResponder {
    return YES;
}

//Pushes random cocktail onto the stack upon detection of a shake
-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.type == UIEventSubtypeMotionShake) {
        
        UINavigationController *vc = (UINavigationController*)self.window.rootViewController;

        [vc popToRootViewControllerAnimated:NO];
        
        //Push randomly selected cocktail onto view stack
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CocktailViewController *c = [storyboard instantiateViewControllerWithIdentifier:@"CocktailViewController"];
        c.cocktail = [self.model getRandomCocktail];
        
        //Push cocktail view controller onto stack
        [(UINavigationController*)self.window.rootViewController pushViewController:c animated:NO];
    }
}

- (void)initializeDatabase {
    
    // use a file manager object to find the location of the database
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // check if the database is in the documents folder
    
    if ([fileManager fileExistsAtPath:self.databasePath]) {
        return;
    }
    
    // otherwise, get path of sql file in application bundle
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseName];
    
    // copy database to documents directory
    
    [fileManager copyItemAtPath:databasePathFromApp toPath:self.databasePath error:nil];
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
