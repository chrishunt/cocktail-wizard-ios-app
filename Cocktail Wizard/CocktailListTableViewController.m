//
//  CocktailListTableViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 15/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailListTableViewController.h"

@interface CocktailListTableViewController ()

@end

@implementation CocktailListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Load model
    self.model = ((AppDelegate*)[UIApplication sharedApplication].delegate).model;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return self.data.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.data objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CocktailCell";
    CocktailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[CocktailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //Get cocktail of cell
    Cocktail *cocktail = [[self.data objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    UIImageView *image = (UIImageView *)[cell viewWithTag:100];
    
    //Load image from bundle or documents folder
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString* filename = [documentsDirectory stringByAppendingPathComponent:cocktail.image];
    image.image = [UIImage imageWithContentsOfFile:filename];
    
    //Set name
    UILabel *cocktailName = (UILabel *)[cell viewWithTag:200];
    cocktailName.text = cocktail.name;
    
    //Set ingredients
    UILabel *cocktailIngredients = (UILabel *)[cell viewWithTag:300];
    cocktailIngredients.text = [cocktail cocktailIngredientsToString];
    cocktailIngredients.numberOfLines = 0;
    
    //Set cocktail property
    cell.cocktail = cocktail;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}

@end