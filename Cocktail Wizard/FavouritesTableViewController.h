//
//  FavouritesTableViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 15/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CocktailListTableViewController.h"

//View controller for a list of the user's favourite cocktails
@interface FavouritesTableViewController : CocktailListTableViewController

@end
