//
//  CocktailViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 15/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "CocktailDataModel.h"
#import "Cocktail.h"
#import "AppDelegate.h"

//View controller managing a individual cocktail
@interface CocktailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

//UI Elements
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *cocktailTitle;
@property (weak, nonatomic) IBOutlet UIImageView *cocktailImage;

//Buttons
@property (weak, nonatomic) IBOutlet UIBarButtonItem *favouriteButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;

//Cocktail properties
@property (weak,nonatomic) CocktailDataModel *model;
@property (strong, nonatomic) Cocktail *cocktail;
@property (strong,nonatomic) NSMutableArray *data;

//Table section header enum
typedef enum StepSections : NSInteger StepSections;
enum StepSections : NSInteger
{
    INGREDIENTS,
    RECIPE
};

//Button actions
-(IBAction)toggleFavourite;
-(IBAction)facebookShare;
-(IBAction)twitterShare;
@end
