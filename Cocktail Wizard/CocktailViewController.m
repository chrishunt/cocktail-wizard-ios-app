//
//  CocktailViewController.m
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 15/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailViewController.h"

@interface CocktailViewController ()

@end

@implementation CocktailViewController

//Returns the number of rows in a section
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.data objectAtIndex:section] count];
}

//Return the number of sections
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.data.count;
}

//Provide relevant section header, enum for readibility
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case INGREDIENTS:
            return @"Ingredients";
            break;
        case RECIPE:
            return @"Recipe";
            break;
        default:
            return @"";
            break;
    }
}

//Resize height of cell for length of contents
//Code from http://www.thelittlebakery.org/2013/01/create-dynamic-height-for-uitableviewcells-in-ios/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *message = [[self.data objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    CGSize maximumLabelSize = CGSizeMake(296,9999);
    UIFont *font = [UIFont systemFontOfSize:17];
    CGSize expectedLabelSize = [message sizeWithFont:font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    NSInteger height = expectedLabelSize.height + 15;
    return height;
}

//Provide contents for ingredient or recipe step
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StepCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //Set text of cell
    cell.textLabel.text = [[self.data objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    //Allow mutiple lines
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    //Prevent highlighting of cell
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//Set scroll view height to match contents
-(void)viewDidLayoutSubviews
{
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setUserInteractionEnabled:YES];
    [self.scrollView setContentSize:CGSizeMake(320, 430+self.tableView.contentSize.height)];
    [self.scrollView setAlwaysBounceVertical:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    //Set cocktail image from either the bundle or documents folder
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString* filename = [documentsDirectory stringByAppendingPathComponent:self.cocktail.image];
    self.cocktailImage.image = [UIImage imageWithContentsOfFile:filename];
    
    //Set cocktail name
    self.cocktailTitle.text = self.cocktail.name;
    
    //Load table with cocktail ingredients and recipe
    self.data = [[NSMutableArray alloc] init];
    [self.data addObject:[self.cocktail getIngredientNames]];
    [self.data addObject:self.cocktail.recipe];
    [self.tableView reloadData];
    
    //Resize table to size of contents
    [self.tableView layoutIfNeeded];
    CGRect frame = self.tableView.frame;
    frame.size.height = self.tableView.contentSize.height;
    self.tableView.frame = frame;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Load model
    self.model = ((AppDelegate*)[UIApplication sharedApplication].delegate).model;
    
    //Add to recent cocktails
    [self.model addToRecent:self.cocktail];
    
    
	//Load relevant favourite image
    if ([self.model isCocktailFavourited:self.cocktail.cocktailID]) {
        self.favouriteButton.image = [UIImage imageNamed:@"star-checked.png"];
    } else {
        self.favouriteButton.image = [UIImage imageNamed:@"star-unchecked.png"];
    }    
}

-(IBAction)toggleFavourite
{
    //Toggle cocktail as a favourite, updating database and icon
    if ([self.model isCocktailFavourited:self.cocktail.cocktailID])
    {
        self.favouriteButton.image = [UIImage imageNamed:@"star-unchecked.png"];
        [self.model toggleFavouriteCocktail:self.cocktail];
    }
    else
    {
        self.favouriteButton.image = [UIImage imageNamed:@"star-checked.png"];
        [self.model toggleFavouriteCocktail:self.cocktail];
    }    
}

-(IBAction)twitterShare
{
    [self shareWithServiceType:SLServiceTypeTwitter];
}

-(IBAction)facebookShare
{
    [self shareWithServiceType:SLServiceTypeFacebook];
}

//Share to facebook or twitter depending on passed servie type
-(void)shareWithServiceType:(NSString*)serviceType
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    if ([SLComposeViewController isAvailableForServiceType:serviceType])
    {
        controller.completionHandler= ^(SLComposeViewControllerResult result) {
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    [self dismissViewControllerAnimated:YES completion:nil];
                    NSLog(@"Cancelled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Posted");
                    break;
            }
            //Dispose of Twitter frame upon completion, as it doesn't do it itself
            if (serviceType == SLServiceTypeTwitter) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        };
        
        //Set message, link and image for share
        NSString *shareText = [NSString stringWithFormat:@"I found this recipe for \"%@\" on Cocktail Wizard", self.cocktail.name];
        NSString *url = [NSString stringWithFormat:@"http://cocktailservice.adampoulston.co.uk/c/%d", self.cocktail.cocktailID];
        
        [controller setInitialText:shareText];
        [controller addURL:[NSURL URLWithString:url]];
        [controller addImage:[UIImage imageNamed:self.cocktail.image]];

        [self presentViewController:controller animated:YES completion:nil];
    }
}
@end
