//
//  RecentCocktailsTableViewController.h
//  Cocktail Wizard
//
//  Created by Christopher Hunt on 22/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import "CocktailListTableViewController.h"

//View controller to represent a list of the ten most recently viewed cocktails by the user
@interface RecentCocktailsTableViewController : CocktailListTableViewController

@end
