//
//  Ingredient.h
//  Cocktail Wizard
//
//  Created by Adam Reece Spencer Poulston on 08/01/2014.
//  Copyright (c) 2014 A&C Software. All rights reserved.
//

#import <Foundation/Foundation.h>

//Class to represent an ingredient
@interface Ingredient : NSObject

@property (assign) NSInteger ingredientID;
@property (strong) NSString* name;
@property (strong) NSString* ingredientType;
@property (strong) NSString* image;
@property (assign) BOOL inBar;

-(id)initWithID:(NSInteger)ingredientID name:(NSString*)name ingredientType:(NSString*)ingredientType image:(NSString*)image inBar:(BOOL)inBar;

@end
